document.getElementById('formulario').addEventListener('submit', (e) => {
    e.preventDefault()
    nombre = document.getElementById('nombre').value;
    apellido = document.getElementById('apellido').value;
    cedula = document.getElementById('cedula').value;
    uni = document.getElementById('uni').value;
    facu = document.getElementById('facu').value;
    carrera = document.getElementById('carrera').value;
    tema = document.getElementById('tema').value;
    tutor = document.getElementById('tutor').value;
    tipo = document.getElementById('tipo').value;


    const parrafo = document.getElementById("msj")
    expReguTex = /^[a-zA-ZÀ-ÿ\s]{1,30}$/;
    expRegNum = /^\d{10}$/;

    let msj = ""
    let ingreso =false

    parrafo.innerHTML =""

    if(!expReguTex.test(nombre)){
        msj += 'Aviso <br>'
        msj += '*Nombre Invalido <br>'
        ingreso= true
    }
    if(!expReguTex.test(apellido)){
        msj += '*Apellido Invalido <br> '
        ingreso= true
    }
    if(!expRegNum.test(cedula)){
        msj += '*Número Invalido <br> '
        ingreso= true
    }
    if(!expReguTex.test(uni)){
        msj += '*Campo Invalido <br> '
        ingreso= true
    }
    if(!expReguTex.test(facu)){
        msj += '*Campo Invalido <br> '
        ingreso= true
    }
    if(!expReguTex.test(carrera)){
        msj += '*Campo Invalido <br> '
        ingreso= true
    }
    if(!expReguTex.test(tema)){
        msj += '*Tema Invalido <br> '
        ingreso= true
    }
    if(!expReguTex.test(tutor)){
        msj += '*Campo Invalido <br> '
        ingreso= true
    }
    if(!expReguTex.test(tipo)){
        msj += '*Tipo Invalido <br> '
        ingreso= true
    }
    if(ingreso){
        parrafo.innerHTML = msj
    }else{
        alert("Registro enviado correctamente...")
    }
})





