var entrar = 1;
function cambiar(link){
	if(entrar==0){
		document.getElementById("sign-in").className = "sign-in salir";
		document.getElementById("sign-up").className = "sign-up mostrar";
		entrar = 1;
		link.innerHTML = "Iniciar Sesión";
	}
	else{
		document.getElementById("sign-in").className = "sign-in mostrar";
		document.getElementById("sign-up").className = "sign-up salir";
		entrar = 0;
		link.innerHTML = "Crear Cuenta";	
	}
}

document.getElementById('sign-in').addEventListener('submit', (e) => {
    e.preventDefault()
    email = document.getElementById('email').value;
    pass = document.getElementById('pass').value;
	
    const parrafo = document.getElementById("msj")
    expReguEmail = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    expRegPass = /^.{4,12}$/;

    let msj = ""
    let ingreso =false

    parrafo.innerHTML =""

    if(!expReguEmail.test(email)){
        msj += 'Aviso <br>'
        msj += '*Email invalido <br>'
        ingreso= true
    }
    if(!expRegPass.test(pass)){
        msj += '*Contraseña Invalida <br> '
        ingreso= true
    }
    
    if(ingreso){
        parrafo.innerHTML = msj
    }else{
        alert("Inicio de sesión correcto...")
    }
})


